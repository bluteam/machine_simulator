package edu.sbuniv.blu;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Hashtable;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

public class AssemblerTest {
	private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
	private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
	Assembler a;

	@Rule
	public TemporaryFolder folder = new TemporaryFolder();

	@Before
	public void setUpStreams() {
		// System.setOut(new PrintStream(outContent));
		// System.setErr(new PrintStream(errContent));
		a = new Assembler();
	}

	@After
	public void cleanUpStreams() {
		// System.setOut(System.out);
		// System.setErr(System.err);
	}

	@Test
	public void testPreprocessMissingBegin() {
		File f = new File("missing_begin.asm");
		try {
			assertEquals(null, a.assemble(f));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testPreprocessMissingEnd() {
		File f = new File("missing_end.asm");
		try {
			assertEquals(null, a.assemble(f));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testPreprocess() {
		ArrayList<String> lines = new ArrayList<String>();
		lines.add("  ");
		lines.add("--comment line");
		lines.add(".BEGIN--comment");
		lines.add("LOAD A --comment--comment");
		lines.add(".END");
		ArrayList<String> code = new ArrayList<String>();
		code.add("LOAD A");
		assertEquals(code, a.preprocess(lines));
	}

	@Test
	public void testPassOne() {
		ArrayList<String> code = Helper.arrayToArrayList(new String[] {
				"LOAD A", 
				"ADD B", 
				"STORE A", 
				"OUT A", 
				"HALT", 
				"A:.DATA 1",
				"B:.DATA 2" 
				});
		Hashtable<String, Integer> symbolTable = new Hashtable<String, Integer>();
		symbolTable.put("A", 5);
		symbolTable.put("B", 6);
		assertEquals(symbolTable, a.passOne(code));
	}

	@Test
	public void testPassTwo() {
		ArrayList<String> code = new ArrayList<String>();
		code.add("LOAD A");
		code.add("ADD B");
		code.add("STORE A");
		code.add("OUT A");
		code.add("HALT");
		code.add("A:.DATA 1");
		code.add("B:.DATA 2");
		Hashtable<String, Integer> symbolTable = a.passOne(code);
		ArrayList<String> expected = new ArrayList<String>();
		expected.add("LOAD 5");
		expected.add("ADD 6");
		expected.add("STORE 5");
		expected.add("OUT 5");
		expected.add("HALT");
		expected.add(".DATA 1");
		expected.add(".DATA 2");
		assertEquals(expected, a.passTwo(code, symbolTable));
	}

	@Test
	public void testGenerateMachineCode() {
		ArrayList<String> code = new ArrayList<String>();
		code.add("LOAD A");
		code.add("ADD B");
		code.add("HALT");
		code.add("A:.DATA 1");
		code.add("B:.DATA 2");
		Hashtable<String, Integer> symbolTable = a.passOne(code);
		ArrayList<String> expected = new ArrayList<String>();
		expected.add("0000000000000011"); // LOAD 3
		expected.add("0011000000000100"); // ADD 4
		expected.add("1111000000000000"); // HALT
		expected.add("0000000000000001"); // .DATA 1
		expected.add("0000000000000010"); // .DATA 2
		assertEquals(expected,
				a.generateMachineCode(a.passTwo(code, symbolTable)));
	}

	@Test
	public void testAssemble() throws IOException {
		File assemblyFile = folder.newFile("program.asm");
		BufferedWriter out = new BufferedWriter(new FileWriter(assemblyFile));
		out.write("--Test program for the assembler\n");
		out.write("--Add two integers and output sum.\n");
		out.write("  .BEGIN\n");
		out.write("LOAD A\n");
		out.write("ADD B\n");
		out.write("STORE A\n");
		out.write("OUT A\n");
		out.write("HALT\n");
		out.write("A:.DATA 1\n");
		out.write("B:.DATA 2\n");
		out.write(".END\n");
		out.close();

		Machine m = Machine.getInstance();
		ConsoleIOServiceStub io = new ConsoleIOServiceStub();
		m.setIOService(io);

		ArrayList<String> code = a.assemble(assemblyFile);
		// for (int i = 0; i < code.size(); i++) {
		// System.out.println("-"+code.get(i)+"-");
		// }
		m.load(code);
		// System.out.println("================");
		// for (int i = 0; i < code.size(); i++) {
		// System.out.println(m.getRAM().readCell(i));
		// }
		m.run();
		assertEquals(3, io.getOutput());
		// assertThat(assemblyFile.exists(), is(true));
	}
}
