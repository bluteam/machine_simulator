package edu.sbuniv.blu;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class ALUTest {
	private Machine m;
	private ALU a;
	private ConsoleIOServiceStub io;

	@Before
	public void runBeforeEveryTest() {
		m = Machine.getInstance();
		a = m.getALU();
		io = new ConsoleIOServiceStub();
		m.setIOService(io);
	}
	
	@Test
	public void testLoad(){
		m.getRAM().writeCell(0, "0101010101010101");
		a.load(0);
		assertEquals(m.getRAM().readCell(0), a.getR());
	}
	
	@Test
	public void testIncrement(){
		m.getRAM().writeCell(0, "0101010101010101");
		a.increment(0);
		assertEquals(m.getRAM().readCell(0), "0101010101010110");
	}
	
	@Test
	public void testStore(){
		m.getRAM().writeCell(0, "0101010101010101");
		a.load(0);
		a.store(1);
		assertEquals(m.getRAM().readCell(1), a.getR());
	}
	
	@Test
	public void testClear(){
		m.getRAM().writeCell(0, "0101010101010101");
		a.clear(0);
		assertEquals(m.getRAM().readCell(0), Machine.ZERO);
	}
	
	@Test
	public void testAdd(){
		m.getRAM().writeCell(0, "0000000000000001");
		a.load(0);
		a.add(0);
		assertEquals("0000000000000010", a.getR());
	}
	
	@Test
	public void testSubtract(){
		m.getRAM().writeCell(0, "0000000000000001");
		a.load(0);
		a.subtract(0);
		assertEquals(Machine.ZERO, a.getR());
	}
	
	@Test
	public void testDecrement(){
		m.getRAM().writeCell(0, "0101010101010101");
		a.decrement(0);
		assertEquals("0101010101010100", m.getRAM().readCell(0));
	}
	
	@Test
	public void testCompare(){
		m.getRAM().writeCell(0, "0101010101010100");
		m.getRAM().writeCell(1, "0101010101010101");
		m.getRAM().writeCell(2, "0101010101010011");
		a.load(0);
		a.compare(1);
		assertEquals(true, a.getGT());
		a.compare(2);
		assertEquals(true, a.getLT());
		a.compare(0);
		assertEquals(true, a.getEQ());
	}
	
	@Test
	public void testJump(){
		a.jump(255);
		assertEquals(255, m.getPC());
	}
	
	@Test
	public void testJumpGT(){
		m.getRAM().writeCell(0, "0101010101010100");
		m.getRAM().writeCell(1, "0101010101010101");
		a.load(0);
		a.compare(1);
		a.jumpGT(255);
		assertEquals(255, m.getPC());
	}
	
	@Test
	public void testJumpLT(){
		m.getRAM().writeCell(0, "0101010101010100");
		m.getRAM().writeCell(1, "0101010101010101");
		a.load(1);
		a.compare(0);
		a.jumpLT(255);
		assertEquals(255, m.getPC());
	}
	
	@Test
	public void testJumpEQ(){
		m.getRAM().writeCell(0, "0101010101010100");
		a.load(0);
		a.compare(0);
		a.jumpEQ(255);
		assertEquals(255, m.getPC());
	}
	
	@Test
	public void testJumpNEQ(){
		m.getRAM().writeCell(0, "0101010101010100");
		m.getRAM().writeCell(1, "0101010101010101");
		a.load(1);
		a.compare(0);
		a.jumpNEQ(255);
		assertEquals(255, m.getPC());
	}
	
	@Test
	public void testIn(){
		io.setInput(255);
		a.in(100);
		assertEquals("0000000011111111", m.getRAM().readCell(100));
	}
	
	@Test
	public void testOut(){
		m.getRAM().writeCell(101, "0000000011111111");
		a.out(101);	
		assertEquals(255, io.getOutput());
	}
	
	@Test
	public void testHalt(){
		a.halt();
		assertEquals(false, m.isRunning());
	}
	@Test
	public void testBinaryToInt(){
		assertEquals(0, ALU.binaryToInt(Machine.ZERO));
		assertEquals(1025, ALU.binaryToInt("0000010000000001"));
	}
	
	@Test
	public void testIntToBinary(){
		assertEquals(Machine.ZERO, ALU.intToBinary(0));
		assertEquals("0000010000000001", ALU.intToBinary(1025));
	}
}
