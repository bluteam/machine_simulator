package edu.sbuniv.blu;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
	ALUTest.class,
	ControllerTest.class,
	MachineTest.class,
	AssemblerTest.class
})

public class AllTests {

}
