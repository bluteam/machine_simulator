package edu.sbuniv.blu;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.FileNotFoundException;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

public class MachineTest {
	private Machine m;
	private RAM ram;
	private ConsoleIOServiceStub io;

	@Before
	public void runBeforeEveryTest() {
		m = Machine.getInstance();
		ram = m.getRAM();
		io = new ConsoleIOServiceStub();
		m.setIOService(io);
	}

	@Test
	public void runLoad() {
		// a+b=c (4+5=9)
		ArrayList<String> code = new ArrayList<String>();
		code.add("0000000000000101"); // 0: load a (5)
		code.add("0011000000000110"); // 1: add b (6)
		code.add("0001000000000111"); // 2: store c (7)
		code.add("1110000000000111"); // 3: out c (7)
		code.add("1111000000000000"); // 4: halt
		code.add("0000000000000100"); // 5: a
		code.add("0000000000000101"); // 6: b
		code.add("0000000000000000"); // 7: c
		m.load(code);
		assertEquals("0000000000000101", ram.readCell(0));
		assertEquals("0011000000000110", ram.readCell(1));
		assertEquals("0001000000000111", ram.readCell(2));
		assertEquals("1110000000000111", ram.readCell(3));
		assertEquals("1111000000000000", ram.readCell(4));
		assertEquals("0000000000000100", ram.readCell(5));
		assertEquals("0000000000000101", ram.readCell(6));
		assertEquals("0000000000000000", ram.readCell(7));
	}

	@Test
	public void testRunSimple() {
		m.run();
		assertEquals(9, io.getOutput());
		ArrayList<String> code = Helper.arrayToArrayList(new String[] {
				"0000000000000101", 
				"0011000000000110", 
				"0001000000000101",
				"1110000000000101", 
				"1111000000000000", 
				"0000000000000001",
				"0000000000000010" });
		m.load(code);
		m.run();
		assertEquals(3, io.getOutput());
	}

	@Test
	public void testRunProgramInFile1() {
		try {
			// 4*5 = 20
			m.load("NONEXISTENT.txt");
			fail("should have thrown an exception.");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testRunProgramInFile2() {
		try {
			// 4*5 = 20
			m.load("program.txt");
			m.run();
			assertEquals(20, io.getOutput());
		} catch (FileNotFoundException e) {
			fail("should have found the file.");
			e.printStackTrace();
		}
	}
}