package edu.sbuniv.blu;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class ControllerTest {
	private Machine m;
	private ALU alu;
	private Controller controller;
	private RAM ram;

	@Before
	public void runBeforeEveryTest() {
		m = Machine.getInstance();
		alu = m.getALU();
		controller = m.getController();
		ram = m.getRAM();
	}
	
	@Test
	public void testFetch(){
		// instruction: load 255
		ram.writeCell(0, "0011000011111111");
		m.setPC(0);
		controller.fetch();
		assertEquals("0011000011111111", m.getIR());
	}
	
	@Test
	public void testDecode(){
		// instruction: load 255
		ram.writeCell(0, "0011000011111111");
		m.setPC(0);
		controller.fetch();
		controller.decode();
		assertEquals(3, controller.getOpIndex());
		assertEquals(255, controller.getOperand());
	}
	
	@Test
	public void testExecute(){
		// instruction: add 255
		ram.writeCell(0, "0011000011111111");
		ram.writeCell(255, "1000000000000001");
		m.setPC(0);
		controller.fetch();
		controller.decode();
		assertEquals(3, controller.getOpIndex());
		assertEquals(255, controller.getOperand());
		alu.reset();
		controller.execute();
		assertEquals("1000000000000001", alu.getR());
	}
}
