package edu.sbuniv.blu;

import java.util.ArrayList;

public class Helper {
	public static ArrayList<String> arrayToArrayList(String[] array){
		ArrayList<String> al = new ArrayList<String>();
		for (int i=0; i<array.length; i++){
			al.add(array[i]);
		}
		return al;
	}
	
	public static void printCode(ArrayList<String> code) {
		System.out.println("===== code listing =====");
		for (int i = 0; i < code.size(); i++) {
			System.out.println(code.get(i));
		}
	}
}
