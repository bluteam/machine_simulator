package edu.sbuniv.blu;

import java.util.Scanner;

public class ConsoleIOService implements IOService {

	@Override
	public int getInput() {
		System.out.println("Enter an integer>");
		Scanner scan = new Scanner(System.in);
		return scan.nextInt();
	}

	@Override
	public void sendOutput(int output) {
		System.out.println("output:"+output);
	}
}
