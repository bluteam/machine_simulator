package edu.sbuniv.blu;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.JToolBar;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.UndoableEditEvent;
import javax.swing.event.UndoableEditListener;
import javax.swing.text.AbstractDocument;
import javax.swing.text.DefaultEditorKit;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoManager;

public class MainFrame extends JFrame {
	JTextPane textPane;
	AbstractDocument doc;
	static final int MAX_CHARACTERS = 300;
	String newline = "\n";
	HashMap<Object, Action> actions;

	// undo helpers
	protected UndoAction undoAction;
	protected RedoAction redoAction;
	protected UndoManager undo = new UndoManager();

	public static void main(String[] args) {
		MainFrame frame = new MainFrame();
	}

	public MainFrame() {
		setSize(600, 400);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JMenuBar menuBar = new JMenuBar();
		this.setJMenuBar(menuBar);
		JMenu fileMenu = new JMenu("File");
		menuBar.add(fileMenu);
		// create two Action Objects
		Action open = new OpenButton("Open", new ImageIcon("open.png"), this);
		Action exit = new ExitButton("Exit", new ImageIcon("exit.png"), this);
		fileMenu.add(open);
		fileMenu.addSeparator();
		fileMenu.add(exit);

		// add same objects to the toolbar
		JToolBar toolbar = new JToolBar();
		getContentPane().add(toolbar, BorderLayout.NORTH);
		// add the two action objects
		toolbar.add(open);
		toolbar.add(exit);
		
		// Create the text pane and configure it.
		textPane = new JTextPane();
		textPane.setCaretPosition(0);
		textPane.setMargin(new Insets(5, 5, 5, 5));
		Document doc = textPane.getDocument();
		JScrollPane scrollPane = new JScrollPane(textPane);
		scrollPane.setPreferredSize(new Dimension(200, 200));

		// Add the components.
		getContentPane().add(scrollPane, BorderLayout.CENTER);

		// Put the initial text into the text pane.
		// initDocument();
		textPane.setCaretPosition(0);

		actions = createActionTable(textPane);
		JMenu editMenu = new JMenu("Edit");
		menuBar.add(editMenu);
		undoAction = new UndoAction();
		editMenu.add(undoAction);
		redoAction = new RedoAction();
		editMenu.add(redoAction);
		editMenu.addSeparator();

		// These actions come from the default editor kit.
		editMenu.add(getActionByName(DefaultEditorKit.cutAction));
		editMenu.add(getActionByName(DefaultEditorKit.copyAction));
		editMenu.add(getActionByName(DefaultEditorKit.pasteAction));
		editMenu.addSeparator();

		editMenu.add(getActionByName(DefaultEditorKit.selectAllAction));

		// Start watching for undoable edits and caret changes.
		doc.addUndoableEditListener(new MyUndoableEditListener());
		// textPane.addCaretListener(caretListenerLabel);
		doc.addDocumentListener(new MyDocumentListener());

		pack();
		setVisible(true);
	}

	// The following two methods allow us to find an
	// action provided by the editor kit by its name.
	private HashMap<Object, Action> createActionTable(
			JTextComponent textComponent) {
		HashMap<Object, Action> actions = new HashMap<Object, Action>();
		Action[] actionsArray = textComponent.getActions();
		for (int i = 0; i < actionsArray.length; i++) {
			Action a = actionsArray[i];
			actions.put(a.getValue(Action.NAME), a);
		}
		return actions;
	}

	private Action getActionByName(String name) {
		return actions.get(name);
	}

	class UndoAction extends AbstractAction {
		public UndoAction() {
			super("Undo");
			setEnabled(false);
		}

		public void actionPerformed(ActionEvent e) {
			try {
				undo.undo();
			} catch (CannotUndoException ex) {
				System.out.println("Unable to undo: " + ex);
				ex.printStackTrace();
			}
			updateUndoState();
			redoAction.updateRedoState();
		}

		protected void updateUndoState() {
			if (undo.canUndo()) {
				setEnabled(true);
				putValue(Action.NAME, undo.getUndoPresentationName());
			} else {
				setEnabled(false);
				putValue(Action.NAME, "Undo");
			}
		}
	}

	class RedoAction extends AbstractAction {
		public RedoAction() {
			super("Redo");
			setEnabled(false);
		}

		public void actionPerformed(ActionEvent e) {
			try {
				undo.redo();
			} catch (CannotRedoException ex) {
				System.out.println("Unable to redo: " + ex);
				ex.printStackTrace();
			}
			updateRedoState();
			undoAction.updateUndoState();
		}

		protected void updateRedoState() {
			if (undo.canRedo()) {
				setEnabled(true);
				putValue(Action.NAME, undo.getRedoPresentationName());
			} else {
				setEnabled(false);
				putValue(Action.NAME, "Redo");
			}
		}
	}

	// This one listens for edits that can be undone.
	protected class MyUndoableEditListener implements UndoableEditListener {
		public void undoableEditHappened(UndoableEditEvent e) {
			// Remember the edit and update the menus.
			undo.addEdit(e.getEdit());
			undoAction.updateUndoState();
			redoAction.updateRedoState();
		}
	}

	// And this one listens for any changes to the document.
	protected class MyDocumentListener implements DocumentListener {
		public void insertUpdate(DocumentEvent e) {
			displayEditInfo(e);
		}

		public void removeUpdate(DocumentEvent e) {
			displayEditInfo(e);
		}

		public void changedUpdate(DocumentEvent e) {
			displayEditInfo(e);
		}

		private void displayEditInfo(DocumentEvent e) {
			Document document = e.getDocument();
			int changeLength = e.getLength();
			System.out.println(e.getType().toString() + ": " + changeLength
					+ " character" + ((changeLength == 1) ? ". " : "s. ")
					+ " Text length = " + document.getLength() + "." + newline);
		}
	}
}
