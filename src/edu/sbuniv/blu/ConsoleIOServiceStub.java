package edu.sbuniv.blu;

public class ConsoleIOServiceStub implements IOService {
	int input;
	int output;
	
	public void setInput(int newInput){
		input = newInput;
	}
	
	public int getOutput(){
		return output;
	}

	@Override
	public int getInput() {
		return input;
	}

	@Override
	public void sendOutput(int newOutput) {
		output = newOutput;
	}
}
