package edu.sbuniv.blu;

import java.awt.event.ActionEvent;

import javax.swing.Icon;
import javax.swing.JFrame;

public class ExitButton extends ActionButton {
	JFrame frame;

	public ExitButton(String caption, Icon img, JFrame parent) {
		super(caption, img);
		frame = parent;
	}

	public void actionPerformed(ActionEvent e) {
		System.exit(0);
	}
}
