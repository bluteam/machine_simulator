package edu.sbuniv.blu;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Scanner;

public class Assembler {
	private ArrayList<String> code;
	private Hashtable<String, Integer> symbolTable;

	public ArrayList<String> assemble(File file) throws FileNotFoundException{
		code = readFile(file);
		code = preprocess(code);
		if (code == null){
			System.err.println("error in preprocessing.");
			return null;
		}		
		System.out.println("After preprocessing ...");
		Helper.printCode(code);
		symbolTable = passOne(code);
		System.out.println("After pass one ...");
		printSymbolTable();
		code = passTwo(code, symbolTable);
		System.out.println("After pass two ...");
		Helper.printCode(code);
		code = generateMachineCode(code);
		return code;
	}
	
	ArrayList<String> readFile(File file) throws FileNotFoundException {
		ArrayList<String> lines = new ArrayList<String>();
		Scanner scan = new Scanner(file);
		while (scan.hasNext()){
			lines.add(scan.nextLine());
		}
		return lines;
	}
	
	Hashtable<String, Integer> passOne(ArrayList<String> code){
		Hashtable<String, Integer> st = new Hashtable<String, Integer>();
		int lineCounter = 0;
		for (int i=0; i<code.size(); i++){
			String[] tokens = code.get(i).split(":");
			if (tokens.length > 1){
				String label = tokens[0];
				st.put(label, lineCounter);
			}
			lineCounter++;
		}
		return st;
	}
	
	ArrayList<String> passTwo(ArrayList<String> code, Hashtable<String, Integer> st){
		ArrayList<String> machineCode = new ArrayList<String>();
		for (int i=0; i<code.size(); i++){
			String line = code.get(i);
			String[] tokens = line.split(":");
			if (tokens.length>1){ // remove label definitions
				tokens[0] = tokens[1];
			}
			tokens = tokens[0].split(" ");
			if (tokens.length>1){
				String label = tokens[1];
				if (st.containsKey(label)){
					machineCode.add(tokens[0]+" "+st.get(label));
				}else if (tokens[0].equals(".DATA")){
					machineCode.add(tokens[0]+" "+tokens[1]);
				}else{
					System.err.println("Label: "+label+" not defined.");
				}
			}else{
				machineCode.add(tokens[0]);
			}
		}
		return machineCode;
	}
	
	/* remove empty, comment, and directive lines */
	ArrayList<String> preprocess(ArrayList<String> lines){
		boolean foundBegin = false;
		boolean foundEnd = false;
		ArrayList<String> code = new ArrayList<String>();
		for (int i=0; i<lines.size(); i++){
			String line = lines.get(i).trim();
			if (line.length() == 0){
				continue; // skip empty lines
			}
			if (line.startsWith("--")){
				continue; // skip comment lines
			}
			String[] tokens = line.split("--"); // remove end of line comments
			if (!foundBegin && tokens[0].equalsIgnoreCase(".BEGIN")){
				foundBegin = true;
				continue; // skip lines before .START
			}else if (foundBegin && tokens[0].equalsIgnoreCase(".BEGIN")){
				System.err.println("Duplicate .BEGIN");
				break;
			}else if (tokens[0].equalsIgnoreCase(".END")){
				foundEnd = true;
				break;
			}else {
				code.add(tokens[0].trim());
			}
		}
		if (!foundBegin){
			System.err.println("Missing .BEGIN");
			return null;
		}else if (!foundEnd){
			System.err.println("Missing .END");
			return null;
		}else{		
			return code;
		}
	}
	
	ArrayList<String> generateMachineCode(ArrayList<String> code){
		System.out.println("in generateMachineCode().");
		ArrayList<String> machineCode = new ArrayList<String>();
		for (int i=0; i<code.size(); i++){
			String line = code.get(i);
			String[] tokens = line.split(" ");
			//System.err.println("line:"+line);
			if (line.equals(OpCode.HALT.toString())){
				machineCode.add(ALU.intToBinary(OpCode.HALT.ordinal()*4*1024/*2^12*/));
			}else if (tokens[0].equalsIgnoreCase(".DATA")){
				machineCode.add(ALU.intToBinary(Integer.parseInt(tokens[1])));
			}else if (tokens.length == 2){
				OpCode opcode = OpCode.valueOf(tokens[0]);
				machineCode.add(
						ALU.intToBinary(opcode.ordinal()*4*1024+Integer.parseInt(tokens[1])));
				
			}else{
				System.err.println("Invalid instruction format:"+line);
			}
		}
		return machineCode;		
	}
	
	public static void main(String[] args){
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter the assembly source file name:");
		String filename = scan.next();
		
		Assembler a = new Assembler();
		ArrayList<String> code;
		try {
			code = a.assemble(new File(filename));
			Helper.printCode(code);
			Machine m = Machine.getInstance();
			m.setIOService(new ConsoleIOService());
			m.load(code);
			m.run();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void printSymbolTable(){
		Enumeration<String> e= symbolTable.keys();
		while(e.hasMoreElements()){
			String key = e.nextElement();
			System.out.println(key+"->"+symbolTable.get(key));
		}
	}
}
