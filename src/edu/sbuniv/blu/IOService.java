package edu.sbuniv.blu;

public interface IOService {
	public int getInput();
	public void sendOutput(int output);
}
