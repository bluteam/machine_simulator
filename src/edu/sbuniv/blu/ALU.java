package edu.sbuniv.blu;

public class ALU {
	private String R = Machine.ZERO;
	private boolean GT = false;
	private boolean LT = false;
	private boolean EQ = false;
	
	public void reset(){
		R = Machine.ZERO;
	}
	
	public void load(int address){
		R = Machine.getInstance().getRAM().readCell(address);
	}
	
	public void store(int address){
		Machine.getInstance().getRAM().writeCell(address, R);
	}
	
	public void clear(int address){
		Machine.getInstance().getRAM().writeCell(address, Machine.ZERO);
	}
	
	public void add(int address){
		int rValue = binaryToInt(R);
		int operand = binaryToInt(Machine.getInstance().getRAM().readCell(address));
		rValue += operand;
		R = intToBinary(rValue);
	}
	
	public void increment(int address){
		int operand = binaryToInt(Machine.getInstance().getRAM().readCell(address));
		operand++;
		Machine.getInstance().getRAM().writeCell(address, intToBinary(operand));
	}
	
	public void subtract(int address){
		int rValue = binaryToInt(R);
		int operand = binaryToInt(Machine.getInstance().getRAM().readCell(address));
		rValue -= operand;
		R = intToBinary(rValue);
	}
	
	public void decrement(int address){
		int operand = binaryToInt(Machine.getInstance().getRAM().readCell(address));
		operand--;
		Machine.getInstance().getRAM().writeCell(address, intToBinary(operand));
	}
	
	public void compare(int address){
		int data = binaryToInt(Machine.getInstance().getRAM().readCell(address));
		if (data > binaryToInt(R)){
			GT = true;
			LT = false;
			EQ = false;
		}else if (data < binaryToInt(R)){
			GT = false;
			LT = true;
			EQ = false;
		}else{
			GT = false;
			LT = false;
			EQ = true;
		}
	}
	
	public void jump(int address){
		Machine.getInstance().setPC(address);
	}
	
	public void jumpGT(int address){
		if (GT){
			Machine.getInstance().setPC(address);
		}
	}
	
	public void jumpLT(int address){
		if (LT){
			Machine.getInstance().setPC(address);
		}
	}
	
	public void jumpEQ(int address){
		if (EQ){
			Machine.getInstance().setPC(address);
		}
	}
	
	public void jumpNEQ(int address){
		if (!EQ){
			Machine.getInstance().setPC(address);
		}
	}
	
	public void in(int address){
		Machine.getInstance().getRAM().writeCell(address, intToBinary(Machine.getInstance().getInput()));
	}
	
	public void out(int address){
		int outputValue = binaryToInt(Machine.getInstance().getRAM().readCell(address));
		Machine.getInstance().setOutput(outputValue);
	}
	
	public void halt(){
		Machine.getInstance().stop();
	}
	
	public String getR(){
		return R;
	}
	
	public boolean getGT(){
		return GT;
	}
	
	public boolean getLT(){
		return LT;
	}
	
	public boolean getEQ(){
		return EQ;
	}
	
	public static int binaryToInt(String binary){
		int result = 0;
		for (int i=0; i<binary.length(); i++){
			result *= 2;
			if (binary.charAt(i) == '1'){
				result++;
			} 
		}
		return result;
	}
	
	public static String intToBinary(int integer){
		StringBuilder sb = new StringBuilder();
		for (int i=0; i<Machine.WORDSIZE; i++){
			int remainder = integer % 2;
			integer /= 2;
			if (remainder == 1){
				sb.append('1');
			}else{
				sb.append('0');
			}
		}
		sb.reverse();
		return sb.toString();
	}
}
