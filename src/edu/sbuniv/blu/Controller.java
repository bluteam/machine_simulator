package edu.sbuniv.blu;

public class Controller {
	public enum OPTYPE {
		LOAD, STORE, CLEAR, ADD, INCEMENT, SUBTRACT, 
		DECREMENT, COMPARE, JUMP, JUMPGT, JUMPEQ, JUMPLT, JUMPNEQ, 
		IN, OUT, HALT
	}
	private int opIndex;
	private int operand;

	int getOpIndex() {
		return opIndex;
	}

	int getOperand() {
		return operand;
	}

	void fetch() {
		int address = Machine.getInstance().getPC();
		String currentInstruction = Machine.getInstance().getRAM()
				.readCell(address);
		//System.err.println("fetch instruction:"+currentInstruction+" at:"+address);
		Machine.getInstance().setIR(currentInstruction);
		Machine.getInstance().setPC(address+1);
	}

	void decode() {
		String currentInstruction = Machine.getInstance().getIR();
		String opcode = currentInstruction.substring(0, 4);
		String data = currentInstruction.substring(4, 16);
		opIndex = 0;
		for (int i = 0; i < 4; i++) {
			opIndex *= 2;
			if (opcode.charAt(i) == '1') {
				opIndex++;
			}
		}
		operand = 0;
		for (int i = 0; i < 12; i++) {
			operand *= 2;
			if (data.charAt(i) == '1') {
				operand++;
			}
		}
		//System.err.println("opcode:"+opIndex+" operand:"+operand);
	}

	void execute() {
		ALU alu = Machine.getInstance().getALU();
		switch (opIndex) {
		case 0:
			alu.load(operand);
			break;
		case 1:
			alu.store(operand);
			break;
		case 2:
			alu.clear(operand);
			break;
		case 3:
			alu.add(operand);
			break;
		case 4:
			alu.increment(operand);
			break;
		case 5:
			alu.subtract(operand);
			break;
		case 6:
			alu.decrement(operand);
			break;
		case 7:
			alu.compare(operand);
			break;
		case 8:
			alu.jump(operand);
			break;
		case 9:
			alu.jumpGT(operand);
			break;
		case 10:
			alu.jumpEQ(operand);
			break;
		case 11:
			alu.jumpLT(operand);
			break;
		case 12:
			alu.jumpNEQ(operand);
			break;
		case 13:
			alu.in(operand);
			break;
		case 14:
			alu.out(operand);
			break;
		case 15:
			alu.halt();
			break;
		}
//		System.err.println("after execute(): R="+Machine.getInstance().getALU().getR());
//		System.err.println("after execute(): CON(17)="+Machine.getInstance().getRAM().readCell(17));
	}

	public void nextCycle() {
		fetch();
		decode();
		execute();
	}
}
