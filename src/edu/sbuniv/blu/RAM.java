package edu.sbuniv.blu;

public class RAM {
	private static final int SIZE = 1024; // 1KB
	private static String[] cells = new String[SIZE]; 

	public String readCell(int address){
		return cells[address];
	}
	
	public void writeCell(int address, String data){
		cells[address] = data;
	}
	
	public void clear(){
		for (int i=0; i<SIZE; i++){
			cells[i] = "0000000000000000";
		}
	}
}
