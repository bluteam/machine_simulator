package edu.sbuniv.blu;

public enum OpCode {
	LOAD,
	STORE,
	CLEAR,
	ADD,
	INCREMENT,
	SUBTRACT,
	DECREMENT,
	COMPARE,
	JUMP,
	JUMPGT,
	JUMPEQ,
	JUMPLT,
	jUMPNEQ,
	IN,
	OUT,
	HALT
}
