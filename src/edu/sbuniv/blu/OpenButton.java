package edu.sbuniv.blu;

import java.awt.event.ActionEvent;

import javax.swing.Icon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;

public class OpenButton extends ActionButton {
	JFrame frame;

	public OpenButton(String caption, Icon img, JFrame parent) {
		super(caption, img);
		
	}

	public void actionPerformed(ActionEvent e) {
		final JFileChooser chooser = new JFileChooser();
		chooser.showOpenDialog(frame);
	}
}
