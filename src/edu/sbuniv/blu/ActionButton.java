package edu.sbuniv.blu;

import java.awt.event.ActionEvent;
import java.util.Hashtable;
import javax.swing.AbstractAction;
import javax.swing.Icon;

public abstract class ActionButton extends AbstractAction{
	Hashtable<String, Object> properties;

	public ActionButton(String caption, Icon img) {
		properties = new Hashtable<String, Object>();
		properties.put(DEFAULT, caption);
		properties.put(NAME, caption);
		properties.put(SHORT_DESCRIPTION, caption);
		properties.put(SMALL_ICON, img);
	}

	public void putValue(String key, Object value) {
		properties.put(key, value);
	}

	public Object getValue(String key) {
		return properties.get(key);
	}

	public abstract void actionPerformed(ActionEvent e);
}
