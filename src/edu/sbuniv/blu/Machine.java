package edu.sbuniv.blu;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Machine {
	public final static int WORDSIZE = 16;
	public final static String ZERO = "0000000000000000";
	private int PC = 0;
	private String IR = ZERO;
	
	private static Machine instance = null;
	private boolean running = false;
	private ALU alu;
	private RAM ram;
	private Controller controller;
	private IOService io;
	
	private Machine(){
		alu = new ALU();
		ram = new RAM();
		controller = new Controller();
	}
	
	public static Machine getInstance(){
		if (instance == null){
			instance = new Machine();
		}
		return instance;
	}
	
	public void setIOService(IOService ioservice){
		io = ioservice;
	}
	
	void stop(){
		running = false;
	}
	
	public boolean isRunning(){
		return running;
	}
	
	int getInput(){
		return io.getInput();
	}
	
	ALU getALU(){
		return alu;
	}
	
	RAM getRAM(){
		return ram;
	}
	
	Controller getController(){
		return controller;
	}
	
	void setOutput(int output){
		io.sendOutput(output);
	}
	
	int getPC(){
		return PC;
	}
	
	void setPC(int newPC){
		PC = newPC;
	}
	
	String getIR(){
		return IR;
	}
	
	void setIR(String newIR){
		IR = newIR;
	}
	
	public void load(ArrayList<String> code){
		for (int i=0; i<code.size(); i++){
			ram.writeCell(i, code.get(i));
		}
	}
	
	public void load(String filename) throws FileNotFoundException{
		ArrayList<String> list = new ArrayList<String>();
		Scanner scan = new Scanner(new File(filename));
		while (scan.hasNext()){
			String line = scan.nextLine();
			if (line.contains("#")){
				String[] tokens = line.split("#");
				String instruction = tokens[0].trim();
				if (instruction.length() > 0){
					list.add(instruction);
				}
			}else {
				list.add(line);
			}
		}
		load(list);
	}
	
	public void run(){
		System.err.println("in run()");
		PC = 0;
		running = true;
		while (running){
			controller.nextCycle();
		}
	}
	
	public static void main(String[] args){
		Scanner scan = new Scanner(System.in);
		System.out.print("Enter machine program file name:");
		String filename = scan.next();
		Machine m = Machine.getInstance();
		m.setIOService(new ConsoleIOService());
		try {
			m.load(filename);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		m.run();
	}
}
