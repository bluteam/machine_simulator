
--Test program for the assembler: 
--output the larger of two integers, continue if the first one is greater.
		.BEGIN
LOAD A
ADD B
STORE A
OUT A
HALT
A:.DATA 1
B:.DATA 2
		.END
LOOP: 	IN		X --read X
		IN		Y --read Y
		LOAD 	X 
		COMPARE Y -- compare Y to X
		JUMPGT	DONE
		OUT		X  -- output X if X >= Y
		JUMP	LOOP
DONE:	OUT		Y -- output Y if Y > X
		HALT
X:		.DATA 	0
Y:		.DATA	0
		.END